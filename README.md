# CPE(Customer-Premises Equipment) Config Generator

> CPE Config Generator takes customer requirements that are for their CPE, populate them to a config template, and generates a custom-made config text file. Strict verifications of IP addresses and others that are from the user inputs are implemented to ensure an accurate customer config. Other convenient functions such as subnet calculation, copy button, and download button are also implemented.

## Run in Local Development

**> *Install Python3.6+ & virtualenv***

**> *Create & enter a virtual environment***

**> *Install dependencies***
  
    pip install -r requirements.txt

**> *Run the app***

    python cpe_config.py

## API Calls

#### Generate configurations of Adtran 908/916/3140/3448, or Cisco 881/891/891f/920

    POST: /generateCPEConfig

###### REQUEST PARAMETERS
*Content-Type: application/json*

    CPEType                               'adtran_9', 'adtran_3140', 'adtran_3448', 'cisco_881', 'cisco_891', 'cisco_891f', or 'cisco_920'
    PublicIPAddress                       True or False
    Switch                                True or False
    Hostname                              'GGxxx-xxxxx-RCs-dd', where x is for one number or one capital letter, s for either A or C, and d for one number
    Location                              '<City>, <State>'
    AccountNumber                         A number string
    PublicIPAddress                       An IP address
    CustomerPublicIPAddress               An IP address from 104.218.180.0/22, 172.85.128.0/17, 134.204.0.0/17, or 134.6.0.0/16
    CustomerPublicMask                    '255.255.255.254', '255.255.255.252', '255.255.255.248', '255.255.255.240', '255.255.255.224', '255.255.255.192', '255.255.255.128', or '255.255.255.0' 
    Loopback10IPAddress                   An IP address from 104.218.180.0/22, 172.85.128.0/17, 134.204.0.0/17, or 134.6.0.0/16
    SubnetMask10                          '255.255.255.255'
    Loopback66IPAddress                   An IP address from 198.18.0.0/16
    SubnetMask66                          '255.255.255.255'

###### SAMPLE RESPONSES
*Content-Type: text/plain*

    # Configurations in plain text format
    ...

#### Generate switch configurations of 1234p or 1531p for Adtran 3140

    POST: /generateSwitchConfig

###### REQUEST PARAMETERS
*Content-Type: application/json*

    switchType               '1234p' or '1531p'
    Hostname                 'GGxxx-xxxxx-RCs-dd', where x is for one number or one capital letter, s for either A or C, and d for one number

###### SAMPLE RESPONSES
*Content-Type: text/plain*

    # Configurations in plain text format
    ...
