from flask import Flask, request, render_template, url_for, make_response

application = app = Flask(__name__)

# CPE_TYPE = ('cisco_881', 'cisco_891', 'adtran_9', 'adtran_3448', 'adtran_3140', 'cisco_891f')
COMMON_PARAMS = ['Hostname', 'Location', 'AccountNumber', 'CustomerPublicIPAddress',
                 'CustomerPublicMask', 'Loopback10IPAddress', 'SubnetMask10',
                 'Loopback66IPAddress', 'SubnetMask66']

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/generateCPEConfig', methods=['POST'])
def generate_config():
    req_data = request.get_json()

    cpe_type = req_data['CPEType']
    public_ip_required = req_data['PublicIPAddress']
    switch_required = req_data['Switch']

    # * for the testing of newly added config templates * 
    # ensure that all parameters needed to generated a template have been sent over via the request
    print('<----------------------------')
    print(req_data)
    print('---------------------------->')
    if cpe_type == 'cisco_920':
        params_expected = (p for p in COMMON_PARAMS if p not in ['Loopback10IPAddress', 'SubnetMask10'])
    else:
        if public_ip_required:
            params_expected = COMMON_PARAMS
        else:
            params_expected = (p for p in COMMON_PARAMS if p not in ['CustomerPublicIPAddress', 'CustomerPublicMask'])
    for param in params_expected:
        if not req_data[param]:
            raise Exception
    
    if cpe_type == 'adtran_9':
        template = render_template('/cpe_config/adtran_908_916',
                                   public_ip_required=public_ip_required, **req_data)
    elif cpe_type == 'adtran_3140':
        template = render_template('/cpe_config/adtran_3140',
                                   public_ip_required=public_ip_required, 
                                   switch_required=switch_required, **req_data)
    elif cpe_type == 'adtran_3448':
        template = render_template('/cpe_config/adtran_3448',
                                   public_ip_required=public_ip_required, **req_data)
    elif cpe_type == 'cisco_881':
        template = render_template('/cpe_config/cisco_881',
                                   public_ip_required=public_ip_required, **req_data)
    elif cpe_type == 'cisco_891':
        template = render_template('/cpe_config/cisco_891',
                                   public_ip_required=public_ip_required, **req_data)
    elif cpe_type == 'cisco_891f':
        template = render_template('/cpe_config/cisco_891_f',
                                   public_ip_required=public_ip_required, **req_data)
    elif cpe_type == 'cisco_920':
        template = render_template('/cpe_config/cisco_920', **req_data)

    response = make_response(template)
    response.headers = {
        'content-type': 'text/plain'
    }
    return response


@app.route('/generateSwitchConfig', methods=['POST'])
def generate_switch_conifg():
    req_data = request.get_json()
    switch_type = req_data['switchType']
    hostname = req_data['Hostname']

    # 'Hostname' is the only parameter expected for switch templates
    if switch_type == '1234p':
        template = render_template('/switch_config/1234p', Hostname=hostname)
    elif switch_type == '1531p':
        template = render_template('/switch_config/1531p', Hostname=hostname)

    response = make_response(template)
    response.headers = {
        'content-type': 'text/plain'
    }
    return response


# @app.route('/test')
# def test():
#     return render_template('test.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='5001', debug=True)
