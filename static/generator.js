$(document).ready(function () {
    // $("#config_modal").modal('show');
    $("#cpe_type").change(function () {

        $("#public_ip, #switch").prop('checked', true);
        $("#public_ip_div, #switch_div, #lia10_tr, #sm10_tr, #cia_div, #cim_div, #switch_type_div").show();
        if ($(this).val() == 'cisco_920') {
            $("#lia10").val('');
            $("#public_ip_div, #switch_div, #lia10_tr, #sm10_tr").hide();
            // $("#cia").trigger('change');
        }
        if ($(this).val() != 'adtran_3140') {
            $("#switch_div, #switch_type_div").hide();
        }
        if ($(this).val().startsWith('c')) {
            $("#hostname").attr('placeholder', 'GGxxx-xxxxx-RCC-dd');
        } else {
            $("#hostname").attr('placeholder', 'GGxxx-xxxxx-RCA-dd');;
        }
        $("#switch_type").val('');
        $("table input, #switch_type").trigger('change');
        $("#pop2_a").attr('data-popover-content', '#pop2');
        // $("#cim").val('255.255.255.0');
    });

    $("#public_ip").change(function () {
        if ($(this).prop('checked')) {
            $("#cia_div, #cim_div").show();
            // $("#cia, #cim").trigger('change');
            // $("option[value='255.255.255.0']").prop('selected', true);
            $("#pop2_a").attr('data-popover-content', '#pop2');
        } else {
            $("#cia_div, #cim_div").hide();
            $("#cia").val('');
            // $("#lia10").trigger('change');
            $("#pop2_a").attr('data-popover-content', '#pop1');
        }
        $("option[value='255.255.255.0']").prop('selected', true);
        $("table input").trigger('change');
    });

    $("#switch").change(function () {
        if ($(this).prop('checked')) {
            $("#switch_type_div").show();
        } else {
            $("#switch_type").val('');
            $("#switch_type_div").hide();
        }
        $("#switch_type").trigger('change');
    });

    $("td a").popover({
        html: true,
        content: function () {
            let content = $(this).attr("data-popover-content");
            return $(content).children(".popover-body").html();
        },
        title: function () {
            let title = $(this).attr("data-popover-content");
            return $(title).children(".popover-heading").html();
        }
    });

    $("#reset_btn").click(function () {
        // $("input").val('');
        // $("#hostname, #location, #account_num, #cia, #cim, #lia10, #sm10, #lia66, #sm66").removeClass('is-invalid').off('change');
        $("#hostname, #location, #account_num, #cia, #cim, #lia10, #lia66, #switch_type").val('').removeClass('is-invalid').off('change');
        $("#cim").val('255.255.255.0');
        // TODO: off -> coupleInpsChange
        $("tr[id=cia_div] img").first().show();
        $("#first_ip_div").hide();
    });

    $("#preview_btn").click(function () {

        // auxiliary function for isValidInp
        let isValidIp = function (ip, ipRangeStart, ipRangeEnd) {
            let IpRegex = /^(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])){3}$/;
            // let IpRegex = /^\d{1,3}(\.\d{1,3}){3}$/;
            if (!IpRegex.test(ip)) {
                return false;
            }

            let ipRangeStartArr = ipRangeStart.split('.');
            let ipRangeEndArr = ipRangeEnd.split('.');
            let ipArr = ip.split('.');
            if (ipArr[0] != ipRangeStartArr[0] || ipArr[1] != ipRangeStartArr[1]) {
                return false;
            }
            let convertToBinary = function (ipP2, ipP3) {
                let arr = [ipP2, ipP3];
                let binaryPP = '';
                arr.forEach(function (item) {
                    let binaryP = parseInt(item).toString(2);
                    let lenP = binaryP.length;
                    if (lenP != 8) {
                        for (let i = 0; i < 8 - lenP; i++) {
                            binaryP = '0' + binaryP;
                        }
                    }
                    binaryPP += binaryP;
                });
                return binaryPP;
            }
            let binaryLast2PStart = convertToBinary(ipRangeStartArr[2], ipRangeStartArr[3]);
            let binaryLast2PEnd = convertToBinary(ipRangeEndArr[2], ipRangeEndArr[3]);
            let binaryLast2PIp = convertToBinary(ipArr[2], ipArr[3]);
            let decimalLast2Ip = parseInt(binaryLast2PIp, 2);
            if (decimalLast2Ip < parseInt(binaryLast2PStart, 2) || decimalLast2Ip >= parseInt(binaryLast2PEnd, 2)) {
                return false;
            } else {
                return true;
            }
        }

        let isValidInp = function (inpId, val) {
            let regex;
            switch (inpId) {
                case 'hostname':
                    if ($("#cpe_type").val().startsWith('a')) {
                        regex = /^GG[A-Z0-9]{3}-[A-Z0-9]{5}-RCA-[0-9]{2}$/;
                    } else {
                        regex = /^GG[A-Z0-9]{3}-[A-Z0-9]{5}-RCC-[0-9]{2}$/;
                    }
                    break;
                case 'location':
                    regex = /^[a-zA-Z]+(\s[a-zA-Z]+)?,\s?[a-zA-Z]+$/;
                    break;
                case 'account_num':
                    regex = /^\d{2,}$/;
                    break;
                case 'cim':
                    return true;
                case 'cia':
                case 'lia10':
                    if (isValidIp(val, '104.218.180.0', '104.218.184.0') ||
                        isValidIp(val, '172.85.128.0', '172.85.255.255') ||
                        isValidIp(val, '134.204.0.0', '134.204.128.0') ||
                        isValidIp(val, '134.6.0.0', '134.6.255.255')) {
                        return true;
                    } else {
                        return false;
                    }
                case 'sm10':
                    return true;
                case 'lia66':
                    return isValidIp(val, '198.18.0.0', '198.18.255.255');
                case 'sm66':
                    return true;
                    case 'switch_type':
                        if (!val) {
                            return false;
                        } else {
                            return true;
                        }
                default:
                    // regex = /^\d{1,3}(\.\d{1,3}){3}$/;
                    // break;
                    return false;
            }
            return regex.test(val);
        }

        let findFirstUsableIp = function (ipAdd, ipMask) {
            let ipAddArr = ipAdd.split('.');
            let ipMaskArr = ipMask.split('.');
            let firstUsableIp = [];
            for (let i = 0; i < ipAddArr.length; i++) {
                firstUsableIp[i] = ipAddArr[i] & ipMaskArr[i];
            }
            if (ipMask != '255.255.255.254') {
                firstUsableIp[3] = parseInt(firstUsableIp[3]) + 1;
            }
            return firstUsableIp.join('.');
        }

        let invalidInpCount = 0;
        for (let i = 0; i < $("table input, #switch_type").length; i++) {
            let currInpId = $("table input, #switch_type")[i].id;
            let isCurrInpValid = isValidInp($("table input, #switch_type")[i].id, $('#' + currInpId).val());
            if (!isCurrInpValid) {
                $('#' + currInpId).addClass('is-invalid');
                if ($("#cpe_type").val() == 'cisco_920' && currInpId == 'lia10') {
                    continue;
                }
                if ($("#cpe_type").val() != 'cisco_920' && !$("#public_ip").prop('checked') && currInpId == 'cia') {
                    continue;
                }
                if ($("#cpe_type").val() != 'adtran_3140' && currInpId == 'switch_type' 
                || $("#cpe_type").val() == 'adtran_3140' && !$("#switch").prop('checked') && currInpId == 'switch_type') {
                    continue;
                }
                invalidInpCount++;
            } else if (!$("#first_ip_div").is(':visible')) {
                $('#' + currInpId).removeClass('is-invalid');
            }
        }

        $("table input, table select, #switch_type").change(function () {
            // if ($(this).prop('id') == 'cia' || $(this).prop('id') == 'cim') {
            $("tr[id=cia_div] img").first().show();
            $("#first_ip_div").hide();
            // $("#lia10").trigger('change');
            // }
            if (!isValidInp($(this).prop('id'), $(this).val())) {
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
            }
            $("#cia, #lia10").trigger('coupleInpsChange');
        });

        let ciaVal = $("#cia").val();
        let cimVal = $("#cim").val();
        if ($("#public_ip").prop('checked') && invalidInpCount == 0) {
            let firstUsableIp = findFirstUsableIp(ciaVal, cimVal);
            console.log(ciaVal, firstUsableIp);
            if (ciaVal != firstUsableIp) {
                // $("tr[id=cia_div] td").addClass('bg-danger');
                $("#cia").addClass('is-invalid');
                $("tr[id=cia_div] img").first().hide();
                $("#first_ip_div").show();
                $("span[id='first_ip_div'] span").html(firstUsableIp + ' is the first usable IP');
                $("span[id='first_ip_div'] button").click(function () {
                    $("#cia").val(firstUsableIp).trigger('change');
                    $("tr[id=cia_div] img").first().show();
                    $("#first_ip_div").hide();
                });
                return;
            } else {
                // $("#cia").removeClass('is-invalid');
                $("tr[id=cia_div] img").first().show();
                $("#first_ip_div").hide();
            }
        }

        if (invalidInpCount != 0) {
            return;
        }

        // check if cia & lia10 are from the same IP range
        if ($("#cpe_type").val() != 'cisco_920' && $("#public_ip").prop('checked')) {
            if (findFirstUsableIp(ciaVal, cimVal) == findFirstUsableIp($("#lia10").val(), cimVal)) {
                $("#cia, #lia10").addClass('is-invalid').on({
                    coupleInpsChange: function () {
                        if (isValidInp('lia10', $("#lia10").val())) {
                            $("#lia10").removeClass('is-invalid');
                        }
                        if (isValidInp('cia', $("#cia").val())) {
                            $("#cia").removeClass('is-invalid');
                        }
                    }
                });
                return;
            } else {
                $("#cia, #lia10").remove('is-invalid');
            }
        }

        // nonlocal - function scope of document.ready
        data = JSON.stringify({
            CPEType: $("#cpe_type").val(),
            Hostname: $("#hostname").val(),
            Location: $("#location").val(),
            AccountNumber: $("#account_num").val(),
            PublicIPAddress: $("#public_ip").prop('checked'),
            CustomerPublicIPAddress: $("#cia").val(),
            CustomerPublicMask: $("#cim").val(),
            Loopback10IPAddress: $("#lia10").val(),
            SubnetMask10: $("#sm10").val(),
            Loopback66IPAddress: $("#lia66").val(),
            SubnetMask66: $("#sm66").val(),
            Switch: $("#switch").prop('checked')
        });
        console.log(data);
        $.post({
            url: '/generateCPEConfig',
            cache: false,
            contentType: 'application/json',
            data: data,
            success: function (result) {
                template = result;
                content = renderPreviewCtn(template);

                // contentS1234p = null;
                // contentS1531p = null;
                contentSwitch = null;
                if ($("#cpe_type").val() == 'adtran_3140' && $("#switch").prop('checked')) {
                    $("#btns_div button").prop('disabled', false).removeClass('bg-secondary');
                    $("button[value='config']").prop('disabled', true).addClass('bg-secondary');
                    let switchType = $("#switch_type").val();
                    $(".btn-group button").val(switchType);
                    $(".btn-group button b").text(switchType);
                    $("#btns_div").show();
                } else {
                    $("#btns_div").hide();
                }

                $("#config_body table").remove();
                $("#config_body").append(content);
                $("#config_modal").modal('show');
            }
        });
    });

    $("#copy_btn").click(function () {
        let temp = template;
        // if ($("#cpe_type").val() == 'adtran_3140' && $("#switch").prop('checked')) {
        //     let disabledBtnVal = $("#btns_div button:disabled").val();
        //     if (disabledBtnVal == '1234p') {
        //         temp = templateS1234p;
        //     } else if (disabledBtnVal == '1531p') {
        //         temp = templateS1531p;
        //     } else {
        //         temp = template;
        //     }
        // } else {
        //     temp = template;
        // }
        if ($("#cpe_type").val() == 'adtran_3140' && $("#switch").prop('checked') &&
            $("#btns_div button:disabled").val() == $("#switch_type").val()) {
            temp = templateSwitch;
        }

        let $tempTA = $('<textarea></textarea>');
        $("body").append($tempTA);
        $tempTA.text(temp).select();
        let indicator = document.execCommand("copy");
        $tempTA.remove();
        if (indicator) {
            $("#copied_msg").show(0).delay(1500).hide(0);
        }
    });

    $("#btns_div button").click(function () {
        if ($(this).prop('disabled')) {
            return;
        }
        $("#btns_div button").prop('disabled', false);
        $(this).prop('disabled', true);

        $("#btns_div button").removeClass('bg-secondary');
        $(this).addClass('bg-secondary');

        $("#config_body table").remove();
        let btnVal = $(this).val();
        let switchType = $("#switch_type").val();
        if (btnVal == switchType && contentSwitch) {
            $("#config_body").append(contentSwitch);
        } else if (btnVal == 'config') {
            $("#config_body").append(content);
        }
        // let switchType = $(this).val();
        // if (switchType == '1234p' && contentS1234p) {
        //     $("#config_body").append(contentS1234p);
        // } else if (switchType == '1531p' && contentS1531p) {
        //     $("#config_body").append(contentS1531p);
        // } else if (switchType == 'config') {
        //     $("#config_body").append(content);
        // } 
        else {
            console.log(switchType);
            $.post({
                url: '/generateSwitchConfig',
                cache: false,
                contentType: 'application/json',
                data: JSON.stringify({
                    switchType: switchType, // '1234p', or '1531p'
                    Hostname: $("#hostname").val()
                }),
                success: function (result) {
                    templateSwitch = result;
                    contentSwitch = renderPreviewCtn(result);
                    // let content = renderPreviewCtn(result);
                    // if (switchType == '1234p') {
                    //     templateS1234p = result;
                    //     contentS1234p = content;
                    // } else if (switchType == '1531p') {
                    //     templateS1531p = result;
                    //     contentS1531p = content;
                    // }
                    // $("#config_body table").remove();
                    $("#config_body").append(contentSwitch);
                    // $("#config_modal").modal('show');
                }
            });
        }
    });

    $("#download_btn").click(function () {
        let urlReq;
        let dataReq;
        let filename;

        let disabledBtnVal = $("#btns_div button:disabled").val();
        if ($("#cpe_type").val() == 'adtran_3140' && $("#switch").prop('checked') &&
            disabledBtnVal != 'config') {
            // let switchType;

            // if (disabledBtnVal == '1234p') {
            //     switchType = '1234p';
            // } else if (disabledBtnVal == '1531p') {
            //     switchType = '1531p';
            // }

            urlReq = '/generateSwitchConfig';
            let hostname = $("#hostname").val();
            dataReq = JSON.stringify({
                // switchType: switchType,
                switchType: disabledBtnVal,
                Hostname: hostname
            });
            // filename = `${hostname}_Switch_${switchType}.txt`;
            filename = `${hostname}_Switch_${disabledBtnVal}.txt`;
        } else {
            urlReq = '/generateCPEConfig';
            dataReq = data;
            filename = `${JSON.parse(data)['Hostname']}_${JSON.parse(data)['CPEType'][0].toUpperCase() + JSON.parse(data)['CPEType'].slice(1)}.txt`;
        }

        $.post({
            url: urlReq,
            cache: false,
            contentType: 'application/json',
            data: dataReq,
            // xhrFields: {
            //     responseType: 'blob'
            // },
            success: function (result) {
                let a = document.createElement('a');
                let url = window.URL.createObjectURL(new Blob([result.replace(/\n/g, '\r\n')], { type: 'text/plain' }));
                a.href = url;
                a.download = filename;
                $("body").append(a);
                a.click();
                a.remove();
                window.URL.revokeObjectURL(url);
                $("#config_modal").modal('hide');
            }
        });
    });


    // function scope of document.ready
    var data;
    var template;
    var content;

    // var templateS1234p;
    // var templateS1531p;
    // var contentS1234p;
    // var contentS1531p;
    var templateSwitch;
    var contentSwitch;

    var renderPreviewCtn = function (result) {
        let result_arr = result.split('\n');
        // console.log(result_arr);
        let content = '<table>'
        for (let i = 0; i < result_arr.length; i++) {
            // prepend white spaces for commands starting with white spaces
            if (result_arr[i].startsWith(' ')) {
                let firstCharIndex;
                for (let j = 0; j < result_arr[i].length; j++) {
                    if (result_arr[i][j] != ' ') {
                        firstCharIndex = j;
                        break;
                    }
                }
                for (let k = 0; k < 4 * firstCharIndex; k++) {
                    result_arr[i] = ' ' + result_arr[i];
                }
            }
            content += '<tr><td style="white-space: pre;">' + result_arr[i] + '</td></tr>';
        }
        content += '</table>'
        return content;
        // $("#config_body table").remove();
        // $("#config_body").append(content);
        // $("#config_modal").modal('show');
    }
});