ARG version=3.7

FROM python:$version

WORKDIR /app

COPY . . 

RUN pip install -r requirements.txt

EXPOSE 5001

CMD ["python", "cpe_config.py"]